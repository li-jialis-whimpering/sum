package com.example.entity;

/**
 * 功能：商品分类信息表
 * 作者：李佳莉
 * 日期：2024/3/12 23:08
 */
public class Type {
    private static final long serialVersionUID = 1L;

    /** ID */
    private Integer id;
    /** 分类名称 */
    private String name;
    /** 分类描述 */
    private String description;
    /** 分类图标 */
    private String img;
}
