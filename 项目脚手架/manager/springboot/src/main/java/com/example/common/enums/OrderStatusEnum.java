package com.example.common.enums;

public enum OrderStatusEnum {
    NO_PAY("待发货"),
//   待发货
    NO_SEND("待收货"),
//    待收货
    NO_RECEIVE(" 待评价"),
//    待评价
    NO_COMMENT("待评价"),
//    完成
    DONE("已完成"),
//    已取消
    CANCEL("已取消"),;
    private  String value;

    OrderStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
