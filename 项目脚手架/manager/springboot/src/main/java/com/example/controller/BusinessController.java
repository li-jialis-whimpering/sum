package com.example.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.example.common.Result;
import com.example.common.enums.ResultCodeEnum;
import com.example.common.enums.RoleEnum;
import com.example.entity.Account;
import com.example.entity.Business;
import com.example.entity.Category;
import com.example.entity.Notice;
import com.example.exception.CustomException;
import com.example.service.BusinessService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商家管理相关接口
 */
@RestController
@RequestMapping("/business")
public class BusinessController {

    @Resource
    private BusinessService businessService;
    @GetMapping("/selectById/{id}")
    public Result selectById(@PathVariable Integer id) {
        Business business  = businessService.selectById(id);
        return Result.success(business);
    }

    @GetMapping("/selectAll")
    public Result selectAll(Business business){
        List<Business> list=businessService.selectAll(business);
        return Result.success(list);
    }

    @PostMapping("/add")
    public Result add(@RequestBody Business business){
        //   检验前端传过来的数据
        if(ObjectUtil.isEmpty(business.getPassword())||ObjectUtil.isEmpty(business.getUsername())){
            throw new CustomException(ResultCodeEnum. PARAM_LOST_ERROR);
        }

        //alt+enter=代码报错提示
        businessService.add(business);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    public Result deleteById(@PathVariable("id") Integer id){
        businessService.deleteById(id);
        return Result.success();
    }

    @DeleteMapping("/delete/batch")
    public Result deleteBatch(@RequestBody List<Integer> ids){
        businessService.deleteBatch(ids);
        return Result.success();
    }


    @PutMapping("/update")
    public Result updateById(@RequestBody Business business){
        businessService.updateById(business);
        return Result.success();
    }

    @GetMapping("/selectPage")
    public Result selectPage(Business business,
                             @RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<Business> page = businessService.selectPage(business, pageNum, pageSize);
        return Result.success(page);
    }







}
