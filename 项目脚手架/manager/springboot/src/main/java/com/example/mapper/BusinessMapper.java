package com.example.mapper;


import com.example.entity.Business;

import java.util.List;

/**
 *操作Business相关数据接口
 */

public interface BusinessMapper {
    List<Business> selectAll(Business business);
//    返回修改的行数
    int insert(Business business);

    int deleteById(Integer id);

    void updateById(Business business);
}
