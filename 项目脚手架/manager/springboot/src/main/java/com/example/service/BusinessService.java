package com.example.service;

import cn.hutool.core.util.ObjectUtil;
import com.example.common.enums.ResultCodeEnum;
import com.example.common.enums.RoleEnum;
import com.example.entity.*;
import com.example.exception.CustomException;
import com.example.mapper.BusinessMapper;
import com.example.utils.TokenUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 *商家业务处理
 */
@Service
public class BusinessService {
    //
    @Resource
    private BusinessMapper businessMapper;
    @Resource
    private CollectService collectService;

    public  Business selectByUsername(String username){
        Business params=new Business();
        params.setUsername(username);
        List<Business> list=this.selectAll(params);
        return list.size()==0 ? null : list.get(0);
    }

    public  Business selectById(Integer id){
        Business params=new Business();
        params.setId(id);
        List<Business> list=this.selectAll(params);
        Business business=list.size()==0 ? null : list.get(0);
        if(business !=null){
            Account currentUser = TokenUtils.getCurrentUser();
            Collect collect = collectService.selectByUserIdAndBuinessId(currentUser.getId(), business.getId());
            business.setIsCollect(collect !=null);
        }

        return business;
    }


    public Integer add(Business business) {
        Business list = selectByUsername(business.getUsername());
        // 如果根据新增数据的账号查询查到了数据  那么这个数据不允许插入，因为账号不能重复
        if(ObjectUtil.isNotEmpty(list)){
            throw new CustomException(ResultCodeEnum.USER_EXIST_ERROR);
        }
        //.name作用是变成字符串
        business.setRole(RoleEnum.BUSINESS.name());
        Integer count=businessMapper.insert(business);
        System.out.println(count);
        return count;
    }


    public List<Business> selectAll(Business business) {
        List<Business> businesses = businessMapper.selectAll(business);

        return businesses;
    }



    public Integer deleteById(Integer id) {
        return businessMapper.deleteById(id);
    }

    public void deleteBatch(List<Integer> ids){
        for(Integer id: ids){
            businessMapper.deleteById(id);
        }
    }

    public void updateById(Business business) {
        //先查询是否有这个商家
        Business business2 =selectById(business.getId());
        if(ObjectUtil.isEmpty(business2)){
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
//    而且这个商家还不是你当前更新的商家
        Business business1 = selectByUsername(business.getUsername());
        if(ObjectUtil.isNotEmpty(business1)&&!Objects.equals(business1.getId(),business.getId())){
            throw new CustomException(ResultCodeEnum.USER_EXIST_ERROR);
        }

        businessMapper.updateById(business);
    }
    public PageInfo<Business> selectPage(Business business, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Business> list = businessMapper.selectAll(business);
        return PageInfo.of(list);
    }


    public void register(Account account) {
        Business business = new Business();
        BeanUtils.copyProperties(account, business);  // 拷贝 账号和密码2个属性
        if (ObjectUtil.isEmpty(account.getName())) {
            business.setName(business.getUsername());
        }
        this.add(business);  // 添加账户信息
    }

    /**
     * 商家登录
     */
    public Account login(Account account) {
        Account dbBusiness = this.selectByUsername(account.getUsername());
        if (ObjectUtil.isNull(dbBusiness)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!account.getPassword().equals(dbBusiness.getPassword())) {   // 比较用户输入密码和数据库密码是否一致
            throw new CustomException(ResultCodeEnum.USER_ACCOUNT_ERROR);
        }
        // 生成token
        String tokenData = dbBusiness.getId() + "-" + RoleEnum.BUSINESS.name();
        String token = TokenUtils.createToken(tokenData, dbBusiness.getPassword());
        dbBusiness.setToken(token);
        return dbBusiness;
    }

    public void updatePassword(Account account) {
        Business business = this.selectByUsername(account.getUsername());
        if (ObjectUtil.isNull(business)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }

        if (!account.getPassword().equals(business.getPassword())) {
            throw new CustomException(ResultCodeEnum.PARAM_PASSWORD_ERROR);
        }
        business.setPassword(account.getNewPassword());
        this.updateById(business);
    }
    /**
     * 检查商家的权限  看看是否可以新增数据
     */
    public void checkBusinessAuth() {
        Account currentUser = TokenUtils.getCurrentUser();  // 获取当前的用户信息
        if (RoleEnum.BUSINESS.name().equals(currentUser.getRole())) {   // 如果是商家 的话
            Business business = selectById(currentUser.getId());
            if (!"通过".equals(business.getStatus())) {   // 未审核通过的商家  不允许添加数据
                throw new CustomException(ResultCodeEnum.NO_AUTH);
            }
        }
    }
}
